<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index(){
        $prds = Product::with('category')->get();
        // dump($prds);

        return view('welcome',compact('prds'));
    }
}
