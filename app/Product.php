<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $table = 'products';
    protected $primaryKey = 'id_product';
    protected $fillable = ['id_ctg'];

    public function category()
    {
        return $this->belongsTo('App\Category','id_ctg');
    }
}
